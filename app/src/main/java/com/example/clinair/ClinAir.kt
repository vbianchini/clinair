package com.example.clinair

import android.app.Application
import com.example.clinair.di.loginModule
import com.example.clinair.di.remoteDataSourceModule
import org.koin.android.ext.android.startKoin

class ClinAir  : Application(){

    override fun onCreate() {
        super.onCreate()
        startKoin(this,listOf(loginModule,remoteDataSourceModule))
    }
}