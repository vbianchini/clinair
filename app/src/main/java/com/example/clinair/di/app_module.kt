package com.example.clinair.di

import com.example.clinair.login.LoginRepository
import com.example.clinair.login.LoginRepositoryImpl
import com.example.clinair.login.LoginViewModel
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val loginModule= module {
    single { LoginRepositoryImpl(get()) as LoginRepository}
    viewModel { LoginViewModel(get(),get()) }
}
