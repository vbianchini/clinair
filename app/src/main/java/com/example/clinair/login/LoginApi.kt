package com.example.clinair.login

import com.example.clinair.model.LoginRequest
import com.example.clinair.model.LoginResponse
import com.example.clinair.utility.Constants
import io.reactivex.Single
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface LoginApi {
    @POST(Constants.LOGIN)
    fun login(@Body loginRequest: LoginRequest) : Deferred<Response<LoginResponse>>
}