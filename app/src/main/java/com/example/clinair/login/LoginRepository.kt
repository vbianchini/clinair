package com.example.clinair.login

import com.example.clinair.model.LoginRequest

interface LoginRepository {
    suspend fun performLogin(Username : String, Password : String) : Boolean
}

class LoginRepositoryImpl(val api : LoginApi) : LoginRepository {
    override suspend fun performLogin(Username: String, Password: String) : Boolean {
       val response =  api.login(LoginRequest(Username,Password)).await()
        return response.body()!!.error.isNullOrEmpty() && !response.body()!!.token.isNullOrEmpty()
    }
}