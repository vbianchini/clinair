package com.example.clinair.login

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.*

class LoginViewModel (val context : Application, val repository: LoginRepository) : AndroidViewModel(context) {

    private val viewModelJob = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    val loginResponse : MutableLiveData<Boolean> = MutableLiveData()

    fun login(Username : String, Password : String){
        uiScope.launch {
            withContext(Dispatchers.IO){
                val login = repository.performLogin(Username,Password)
                if(login) loginResponse.postValue(true)
            }
        }
    }
}