package com.example.clinair.model

data class LoginRequest (
    val username : String,
    val password : String
)