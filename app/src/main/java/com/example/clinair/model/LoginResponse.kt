package com.example.clinair.model

data class LoginResponse (
    val error : String?,
    val token : String?
)